import pyrebase
from django.shortcuts import render

config = {

'apiKey': "AIzaSyDZuyPrBl7Wn-dRhsQK1zJNJimdVzX0AtU",
    'authDomain': "python-firebase-project-bdd.firebaseapp.com",
    'databaseURL': "https://python-firebase-project-bdd-default-rtdb.europe-west1.firebasedatabase.app",
    'projectId': "python-firebase-project-bdd",
    'storageBucket': "python-firebase-project-bdd.appspot.com",
    'messagingSenderId': "922075991606",
    'appId': "1:922075991606:web:96ca54165271111ba31297",
    'measurementId': "G-3P06JNMSX8"    
  }

firebase = pyrebase.initialize_app(config)

auth = firebase.auth()
db = firebase.database()

def signIn(request):

    return render(request, "signin.html")

def connexion(request):
    email=request.POST.get('email')
    passw = request.POST.get("pass")
    try:
        user = auth.sign_in_with_email_and_password(email,passw)
    except:
        message = "invalid cerediantials"
        return render(request,"signin.html",{"msg":message})
    print(user)
    return render(request, "welcome.html",{"e":email})

def getTodo(request):
    all_todos = [] # empty array
    todos = db.child("todos").get() #get data

    for todo in todos.each() or []: #loop in all todo tasks
        all_todos.append([todo.key(), todo.val()]) # Add to this array the key val of my data

    return render(request, "todo.html", {"all_todos": all_todos})

def postTodo(request):
  todos = getTodo()


data = {"task": "Ceci est un autre test"}
db.child("todos").push(data)
